package com.example.explore;

import android.app.Application;

import com.example.explore.utils.FileUtils;

public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //creates the root dir once the application is loaded
        FileUtils.createRootDir(this);
    }
}
