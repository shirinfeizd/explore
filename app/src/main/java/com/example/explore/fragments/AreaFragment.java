package com.example.explore.fragments;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.explore.CV;
import com.example.explore.Handler.ITouchGestureHandler;
import com.example.explore.R;
import com.example.explore.Wrapper.TouchableWrapper;
import com.example.explore.activities.Area;
import com.example.explore.activities.OrderExperimentActivity;
import com.example.explore.utils.LoggerUtil;
import com.example.explore.utils.MapUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import org.opencv.android.OpenCVLoader;

import java.io.IOException;

public class AreaFragment extends SupportMapFragment implements ITouchGestureHandler, GoogleMap.OnCameraIdleListener,
        GoogleMap.OnMapClickListener, OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {
    private static final String TAG = AreaFragment.class.getSimpleName();

    public View mOriginalContentView;
    public TouchableWrapper mTouchView;
    private GoogleMap mMap;
    private Bitmap mBitmap;
    private Projection mProjection;
    private CV mCV;
    private Area mArea;
    private MapUtil mMapUtil;
    Bundle mExtras = null;
    private float mPrevZoomLevel = 0;
    private LoggerUtil mLoggerUtil;

    // Load OpenCV before onCreate gets called to avoid initialization errors/crashes.
    static {
        if (!OpenCVLoader.initDebug()) {
            // Handle initialization error
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        mOriginalContentView = super.onCreateView(inflater, parent, savedInstanceState);
        mTouchView = new TouchableWrapper(getActivity(), this);
        mTouchView.addView(mOriginalContentView);
        return mTouchView;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        getMapAsync(this);
        mCV = new CV(getContext());
        mArea = (Area) getActivity();
        mMapUtil = new MapUtil(getContext());
        mLoggerUtil = new LoggerUtil(
                getContext(),
                mExtras.getString(OrderExperimentActivity.EXPERIMENT),
                mExtras.getString(OrderExperimentActivity.USER_ID));
    }

    @Override
    public View getView() {
        return mOriginalContentView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            // set the custom styling to the map.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.area_style_json));

            if (!success) {
                mLoggerUtil.log("Style parsing failed.");
                Log.e(TAG, "Style parsing failed.");
            } else {
                mLoggerUtil.log("Style added.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
            mLoggerUtil.log("Can't find style. Error: ");
        }

        mMap = googleMap;
        LatLng currLocation = new LatLng(mExtras.getDouble(OrderExperimentActivity.LATITUDE), mExtras.getDouble(OrderExperimentActivity.LONGITUDE));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currLocation, mExtras.getFloat(OrderExperimentActivity.ZOOM_LEVEL)));
        mMap.setOnMapClickListener(this);
        mMap.setOnCameraIdleListener(this);
        mMap.setOnMapLoadedCallback(this);
        mProjection = mMap.getProjection();
    }

    private void CaptureMapScreen() {
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                mBitmap = snapshot;
                mCV.setBitmap(mBitmap, mMap.getCameraPosition().zoom);
                mBitmap = mCV.getBitmap();
                mArea.updateImageView(mBitmap);
                mProjection = mMap.getProjection();
            }
        };

        mMap.snapshot(callback);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        try {
            mLoggerUtil.log("Map Click. Coordinates(lat,long): (" + latLng.latitude + "," + latLng.longitude + ")");
            processLatLng(latLng);
            CaptureMapScreen();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processLatLng(LatLng latLng) throws IOException {
        //provide feedback to the user stating the address of the selected point.
        addHapticAndVoiceFeedback(latLng);

        //convert latlng to point
        Point screenPosition = mProjection.toScreenLocation(latLng);

        Log.d(TAG, screenPosition.x + " " + screenPosition.y);

        //add marker to that position
        mMap.addMarker(new MarkerOptions()
                .position(latLng));
    }

    private void addHapticAndVoiceFeedback(LatLng latLng) throws IOException {
        mMapUtil.addHapticFeedback(100);
        mMapUtil.addVoiceFeedback(mMapUtil.getStreetNameFromLatLng(latLng));
    }

    @Override
    public void onCameraIdle() {
        float currentZoomLevel = mMap.getCameraPosition().zoom;
        if (mPrevZoomLevel != 0 && currentZoomLevel != mPrevZoomLevel) {
            String zoomTag = getString(R.string.zoomed_out_to);
            if (currentZoomLevel > mPrevZoomLevel) {
                zoomTag = getString(R.string.zoomed_in_to);
            }
            mMapUtil.addVoiceFeedback(String.format(
                    zoomTag,
                    Integer.toString((int) currentZoomLevel)));
        } else if (mPrevZoomLevel == currentZoomLevel) {
            mMapUtil.addVoiceFeedback(getString(R.string.map_moved));
        }

        mPrevZoomLevel = currentZoomLevel;
        if (mBitmap == null) {
            return;
        }
        // idle gets fired before the map could get loaded.
        // So a simple hack to set the listener here as onMapLoaded gets called only once.
        mMap.setOnMapLoadedCallback(this);
    }

    @Override
    public void onMapLoaded() {
        if (mBitmap == null) {
            return;
        }

        mMap.clear();
        CaptureMapScreen();
    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void enableSwipeGesture() {

    }

    @Override
    public void disableSwipeGesture() {

    }

    @Override
    public void onDoubleTap() {

    }

    @Override
    public void touchExplore(Point point) {

    }

    public void setExtras(Bundle bundle) {
        mExtras = bundle;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        super.onDestroy();
        mMapUtil.releaseUtils();
        mLoggerUtil.log("Experiment done!");
        mLoggerUtil.close();
    }
}
