package com.example.explore.fragments;

import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewGroup;

import com.example.explore.Handler.ITaskUpdate;
import com.example.explore.Handler.ITouchGestureHandler;
import com.example.explore.R;
import com.example.explore.Task.NetworkTask;
import com.example.explore.Wrapper.TouchableWrapper;
import com.example.explore.activities.OrderExperimentActivity;
import com.example.explore.activities.POI;
import com.example.explore.model.Element;
import com.example.explore.model.MapData;
import com.example.explore.utils.CommonUtil;
import com.example.explore.utils.LoggerUtil;
import com.example.explore.utils.MapUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import hu.supercluster.overpasser.library.output.OutputModificator;
import hu.supercluster.overpasser.library.output.OutputOrder;
import hu.supercluster.overpasser.library.output.OutputVerbosity;
import hu.supercluster.overpasser.library.query.OverpassQuery;

import static hu.supercluster.overpasser.library.output.OutputFormat.JSON;

public class POIFragment extends SupportMapFragment implements OnMapReadyCallback,
        GoogleMap.OnCameraIdleListener,
        ITaskUpdate,
        ITouchGestureHandler,
        GoogleMap.OnMarkerClickListener {
    private static final float ZOOM_THRESHOLD = 15.0f;
    private static final String TAG = POIFragment.class.getSimpleName();

    public View mOriginalContentView;
    public TouchableWrapper mTouchView;
    private GoogleMap mMap;
    private MapUtil mMapUtil = null;
    private float mPrevZoomLevel = 0;
    private POI mPOI;
    private Integer mIteratorIndexOfPOI = null;
    private List<Element> mPOIList;
    private Marker mCurrentMarker = null;
    private Element mLastChosenPOI = null;
    private List<Point> mPOISortedList = null;
    private Bundle mExtras = null;
    private NetworkTask mNetworkTask;
    private LoggerUtil mLoggerUtil;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        mOriginalContentView = super.onCreateView(inflater, parent, savedInstanceState);
        mTouchView = new TouchableWrapper(getActivity(), this);
        mTouchView.addView(mOriginalContentView);
        return mTouchView;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        getMapAsync(this);
        mMapUtil = new MapUtil(getContext());
        mPOI = (POI) getActivity();
        mLoggerUtil = new LoggerUtil(
                getContext(),
                mExtras.getString(OrderExperimentActivity.EXPERIMENT),
                mExtras.getString(OrderExperimentActivity.USER_ID));
    }

    @Override
    public View getView() {
        return mOriginalContentView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        try {
            // set the custom styling to the map.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
                mLoggerUtil.log("Style parsing failed.");
            } else {
                mLoggerUtil.log("Style added.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
            mLoggerUtil.log("Can't find style.");
        }

        LatLng currLocation = new LatLng(mExtras.getDouble(OrderExperimentActivity.LATITUDE), mExtras.getDouble(OrderExperimentActivity.LONGITUDE));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currLocation, mExtras.getFloat(OrderExperimentActivity.ZOOM_LEVEL)));
        mMap.setOnCameraIdleListener(this);
        mMap.setOnMarkerClickListener(this);
        disableSwipeGesture();
    }

    @Override
    public void onCameraIdle() {
        float currentZoomLevel = mMap.getCameraPosition().zoom;
        Log.d(TAG, "onCameraIdle: Zoom level : " + currentZoomLevel);
        if (currentZoomLevel > ZOOM_THRESHOLD) {
            mPOI.showProgressBar();
            if (mPrevZoomLevel != 0 && currentZoomLevel != mPrevZoomLevel) {
                String zoomTag = getString(R.string.zoomed_out_to);
                if (currentZoomLevel > mPrevZoomLevel) {
                    zoomTag = getString(R.string.zoomed_in_to);
                }
                mMapUtil.addVoiceFeedback(String.format(
                        zoomTag,
                        Integer.toString((int) currentZoomLevel)));
            } else if (mPrevZoomLevel == currentZoomLevel) {
                mMapUtil.addVoiceFeedback(getString(R.string.map_moved));
            }

            mPrevZoomLevel = currentZoomLevel;
            mLoggerUtil.log("Current Zoom level: " + mPrevZoomLevel);

            Projection projection = mMap.getProjection();
            LatLngBounds bounds = projection.getVisibleRegion().latLngBounds;
            LatLng northEast = bounds.northeast;
            LatLng southWest = bounds.southwest;

            mMapUtil.setProjection(projection);

            String query = new OverpassQuery()
                    .format(JSON)
                    .timeout(300)
                    .filterQuery()
                    .node()
                    .boundingBox(southWest.latitude, southWest.longitude, northEast.latitude, northEast.longitude)
                    .end()
                    .output(OutputVerbosity.BODY, OutputModificator.GEOM, OutputOrder.QT, Integer.MAX_VALUE)
                    .build();

            mNetworkTask = new NetworkTask(this);
            mNetworkTask.execute(query);

        } else {
            mLoggerUtil.log("Current zoom level less than threshold! Zoom level: " + currentZoomLevel);
            mMapUtil.addVoiceFeedback(getResources().getString(R.string.zoom_in_more));
        }
        disableSwipeGesture();
    }

    @Override
    public void processStarted() {
        if (mNetworkTask.isCancelled()) {
            return;
        }

        mLoggerUtil.log(getString(R.string.start_process));
        mMapUtil.addVoiceFeedback(getString(R.string.start_process));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void processCompleted(MapData mapData) {
        if (mNetworkTask.isCancelled()) {
            return;
        }

        mMapUtil.populateCategoricalDataFromMap(mapData);
        mMapUtil.populateIntersectingPointsOfStreets(mapData);
        mPOIList = mapData.getPoiList();
        mPOISortedList = mMapUtil.sortPOIList(mMapUtil.getPOICoordinates(mapData.getPoiList()), 4);
        mPOI.hideProgressBar();
        mIteratorIndexOfPOI = -1;

        for (Element element : mPOIList) {
            String typeAmenity = element.getTag().getAmenity();
            String typeShop = element.getTag().getShop();
            String type = typeAmenity != null ? typeAmenity : typeShop;
            addMarker(new LatLng(element.getLatitude(), element.getLongitude()), type);
        }

        mMapUtil.addVoiceFeedback(getString(R.string.end_process));
        mLoggerUtil.log(getString(R.string.end_process));
    }

    private void checkLatlngFallsWithInAPOIArea(LatLng latLng, String type) {
        Element poi = mMapUtil.checkLatLngFallsWithinBoundsOfPOI(latLng, mPOIList);
        if (poi != null && (mLastChosenPOI != poi || type.equals("click"))) {
            mLastChosenPOI = poi;
            mIteratorIndexOfPOI = mPOISortedList.indexOf(mMapUtil.convertLatlngToPoint(new LatLng(poi.getLatitude(), poi.getLongitude())));
            addHapticAndVoiceFeedback(SoundEffectConstants.CLICK);
        } else if (mLastChosenPOI != null) {
            // when the point does not lie within the area of POI,
            // check the distance between the current point and the last POI point
            // when the distance is above the threshold then reset the last POI point to null.
            Point p1 = mMapUtil.convertLatlngToPoint(latLng);
            Point p2 = mMapUtil.convertLatlngToPoint(new LatLng(mLastChosenPOI.getLatitude(), mLastChosenPOI.getLongitude()));
            Log.d(TAG, "Distance: " + CommonUtil.distanceBetweenTwoPoints(p1, p2));
            if (CommonUtil.distanceBetweenTwoPoints(p1, p2) > 150) {
                mLastChosenPOI = null;
            }
        }
    }

    @Override
    public void swipeLeft() {
        if (mPOIList == null) {
            return;
        }

        mLoggerUtil.log("Swiped Left");
        if (mIteratorIndexOfPOI == -1) {
            mIteratorIndexOfPOI = 0;
        } else if (mIteratorIndexOfPOI == 0) {
            mIteratorIndexOfPOI = mPOISortedList.size() - 1;
        } else {
            mIteratorIndexOfPOI--;
        }
        Log.d(TAG, "swipeLeft: POIIndex: " + mIteratorIndexOfPOI);
        addHapticAndVoiceFeedback(SoundEffectConstants.NAVIGATION_LEFT);
    }

    @Override
    public void swipeRight() {
        if (mPOIList == null) {
            return;
        }

        mLoggerUtil.log("Swiped Right");
        mIteratorIndexOfPOI++;
        mIteratorIndexOfPOI = (mIteratorIndexOfPOI) % mPOISortedList.size();
        Log.d(TAG, "swipeRight: POIIndex: " + mIteratorIndexOfPOI);
        addHapticAndVoiceFeedback(SoundEffectConstants.NAVIGATION_RIGHT);
    }

    private void addHapticAndVoiceFeedback(int soundConstant) {
        removeMarker();
        Element element = mMapUtil.getPOICorrespondingToPoint(mPOISortedList.get(mIteratorIndexOfPOI), mPOIList);
        mLoggerUtil.log("Selected POI: " + mMapUtil.getPoiName(element));
        mOriginalContentView.playSoundEffect(soundConstant);
        mMapUtil.addVoiceFeedback((mMapUtil.getPoiName(element)));
        mMapUtil.addHapticFeedback(30);
        addMarker(new LatLng(element.getLatitude(), element.getLongitude()), null);
    }

    @Override
    public void enableSwipeGesture() {
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
    }

    @Override
    public void disableSwipeGesture() {
        mMap.getUiSettings().setZoomGesturesEnabled(false);
        mMap.getUiSettings().setScrollGesturesEnabled(false);
    }

    @Override
    public void onDoubleTap() {
        mMap.getUiSettings().setZoomGesturesEnabled(false);
        CommonUtil.Toast(getContext(), "Double Tap");
    }

    @Override
    public void touchExplore(Point point) {
        disableSwipeGesture();
        //convert point to latlng
        LatLng latLng = mMapUtil.convertPointToLatlng(point);
        checkLatlngFallsWithInAPOIArea(latLng, "explore");
        mLoggerUtil.log("Touch and Explore. Coordinates(lat,lng): (" + latLng.latitude + "," + latLng.longitude + ")");
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (mPrevZoomLevel <= ZOOM_THRESHOLD) {
            return true;
        }

        LatLng latLng = marker.getPosition();
        mLoggerUtil.log("Map Click. Coordinates(lat,long): (" + latLng.latitude + "," + latLng.longitude + ")");
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        checkLatlngFallsWithInAPOIArea(latLng, "click");
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapUtil.releaseUtils();
        mNetworkTask.cancel(true);
        mLoggerUtil.log("Experiment done!");
        mLoggerUtil.close();
    }

    private void addMarker(LatLng latLng, String type) {
        MarkerOptions markerOptions;

        if (type == null) {
            markerOptions = new MarkerOptions().position(latLng);
            mCurrentMarker = mMap.addMarker(markerOptions);
        } else {
            markerOptions = new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(mMapUtil.getIconBasedOnType(type)));
            mMap.addMarker(markerOptions);
        }
    }

    private void removeMarker() {
        if (mCurrentMarker == null) {
            return;
        }
        mCurrentMarker.remove();
    }

    public void setExtras(Bundle bundle) {
        mExtras = bundle;
    }
}
