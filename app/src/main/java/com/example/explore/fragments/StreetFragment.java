package com.example.explore.fragments;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewGroup;

import com.example.explore.Handler.ITaskUpdate;
import com.example.explore.Handler.ITouchGestureHandler;
import com.example.explore.R;
import com.example.explore.Task.NetworkTask;
import com.example.explore.Wrapper.TouchableWrapper;
import com.example.explore.activities.OrderExperimentActivity;
import com.example.explore.activities.Street;
import com.example.explore.model.MapData;
import com.example.explore.utils.CommonUtil;
import com.example.explore.utils.LoggerUtil;
import com.example.explore.utils.MapUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import hu.supercluster.overpasser.library.output.OutputModificator;
import hu.supercluster.overpasser.library.output.OutputOrder;
import hu.supercluster.overpasser.library.output.OutputVerbosity;
import hu.supercluster.overpasser.library.query.OverpassQuery;

import static hu.supercluster.overpasser.library.output.OutputFormat.JSON;

public class StreetFragment extends SupportMapFragment implements OnMapReadyCallback,
        GoogleMap.OnCameraIdleListener,
        GoogleMap.OnMapClickListener,
        ITaskUpdate,
        ITouchGestureHandler {
    private static final float ZOOM_THRESHOLD = 15.0f;
    private static final String TAG = StreetFragment.class.getSimpleName();

    public View mOriginalContentView;
    public TouchableWrapper mTouchView;
    private GoogleMap mMap;
    private MapData mMapData = null;
    private MapUtil mMapUtil = null;
    private float mPrevZoomLevel = 0;
    private Map<String, List<List<LatLng>>> mStreetsCoordinates;
    private Street mStreet;
    private int mIteratorIndexOfStreet = 0;
    private String mLastChosenStreetName = null;
    Bundle mExtras = null;
    private NetworkTask mNetworkTask;
    List<String> mStreetNames = null;
    private LoggerUtil mLoggerUtil;
    private LatLng mLastIntersectionPoint;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        mOriginalContentView = super.onCreateView(inflater, parent, savedInstanceState);
        mTouchView = new TouchableWrapper(getActivity(), this);
        mTouchView.addView(mOriginalContentView);
        return mTouchView;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        getMapAsync(this);
        mMapUtil = new MapUtil(getContext());
        mStreet = (Street) getActivity();
        mLoggerUtil = new LoggerUtil(
                getContext(),
                mExtras.getString(OrderExperimentActivity.EXPERIMENT),
                mExtras.getString(OrderExperimentActivity.USER_ID));
    }

    @Override
    public View getView() {
        return mOriginalContentView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            // set the custom styling to the map.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
                mLoggerUtil.log("Style parsing failed.");
            } else {
                mLoggerUtil.log("Style added.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
            mLoggerUtil.log("Can't find style.");
        }

        mMap = googleMap;
        LatLng currLocation = new LatLng(mExtras.getDouble(OrderExperimentActivity.LATITUDE), mExtras.getDouble(OrderExperimentActivity.LONGITUDE));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currLocation, mExtras.getFloat(OrderExperimentActivity.ZOOM_LEVEL)));
        mMap.setOnCameraIdleListener(this);
        mMap.setOnMapClickListener(this);
        disableSwipeGesture();
    }

    @Override
    public void onCameraIdle() {
        float currentZoomLevel = mMap.getCameraPosition().zoom;
        Log.d(TAG, "onCameraIdle: Zoom level : " + currentZoomLevel);
        if (currentZoomLevel > ZOOM_THRESHOLD) {
            mStreet.showProgressBar();
            if (mPrevZoomLevel != 0 && currentZoomLevel != mPrevZoomLevel) {
                String zoomTag = getString(R.string.zoomed_out_to);
                if (currentZoomLevel > mPrevZoomLevel) {
                    zoomTag = getString(R.string.zoomed_in_to);
                }
                mMapUtil.addVoiceFeedback(String.format(
                        zoomTag,
                        Integer.toString((int) currentZoomLevel)));
            } else if (mPrevZoomLevel == currentZoomLevel) {
                mMapUtil.addVoiceFeedback(getString(R.string.map_moved));
            }

            mPrevZoomLevel = currentZoomLevel;
            mLoggerUtil.log("Current Zoom level: " + mPrevZoomLevel);

            Projection projection = mMap.getProjection();
            LatLngBounds bounds = projection.getVisibleRegion().latLngBounds;
            LatLng northEast = bounds.northeast;
            LatLng southWest = bounds.southwest;

            mMapUtil.setProjection(projection);

            String query = new OverpassQuery()
                    .format(JSON)
                    .timeout(300)
                    .filterQuery()
                    .node()
                    .boundingBox(southWest.latitude, southWest.longitude, northEast.latitude, northEast.longitude)
                    .end()
                    .output(OutputVerbosity.BODY, OutputModificator.GEOM, OutputOrder.QT, Integer.MAX_VALUE)
                    .build();

            try {
                mNetworkTask = new NetworkTask(this);
                mNetworkTask.execute(query);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, e.getMessage());
            }
        } else {
            mLoggerUtil.log("Current zoom level less than threshold! Zoom level: " + currentZoomLevel);
            mMapUtil.addVoiceFeedback(getResources().getString(R.string.zoom_in_more));
        }
        disableSwipeGesture();
    }

    @Override
    public void processStarted() {
        if (mNetworkTask.isCancelled()) {
            return;
        }

        mLoggerUtil.log(getString(R.string.start_process));
        mMapUtil.addVoiceFeedback(getString(R.string.start_process));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void processCompleted(MapData mapData) {
        if (mNetworkTask.isCancelled()) {
            return;
        }

        mMapData = mapData;
        mMapUtil.populateCategoricalDataFromMap(mMapData);
        mMapUtil.populateIntersectingPointsOfStreets(mMapData);
        plotStreets();
        mStreet.hideProgressBar();
        mMapUtil.addVoiceFeedback(getString(R.string.end_process));
        mStreetNames = new ArrayList<>(mStreetsCoordinates.keySet());
        mLoggerUtil.log(getString(R.string.end_process));
    }

    private void plotStreets() {
        mStreetsCoordinates = mMapUtil.getStreetCoordinates(mMapData);

        if (mStreetsCoordinates == null) {
            return;
        }

        for (String streetName : mStreetsCoordinates.keySet()) {

            Log.d(TAG, "plotStreets: " + streetName);
            List<List<LatLng>> coordinates = mStreetsCoordinates.get(streetName);

            if (coordinates == null) {
                return;
            }

            for (List<LatLng> part : coordinates) {
                Polyline line = mMap.addPolyline(new PolylineOptions().width(10).color(Color.RED));
                line.setPoints(part);
            }
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (mPrevZoomLevel <= ZOOM_THRESHOLD) {
            return;
        }

        mLoggerUtil.log("Map Click. Coordinates(lat,long): (" + latLng.latitude + "," + latLng.longitude + ")");
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        checkLatlngFallsWithinStreetArea(latLng, "click");
    }

    private void checkLatlngFallsWithinStreetArea(LatLng latLng, String type) {
        mStreetsCoordinates = mMapUtil.getStreetCoordinates(mMapData);

        String streetName = mMapUtil.checkLatLngFallsWithinBoundsOfAStreet(latLng, mStreetsCoordinates);
        if (streetName != null && (!streetName.equals(mLastChosenStreetName) || type.equals("click"))) {
            mLastChosenStreetName = streetName;
            addHapticAndVoiceFeedback(SoundEffectConstants.CLICK, 30, mLastChosenStreetName);
        } else if (streetName != null) {
            addHapticAndVoiceFeedback(-1, 10, null);
        } else if (mLastChosenStreetName != null) {
            addHapticAndVoiceFeedback(SoundEffectConstants.CLICK, 30, null);
            mLastChosenStreetName = null;
        }

        checkLatlngFallsWithinIntersectionArea(latLng);
    }

    private void checkLatlngFallsWithinIntersectionArea(LatLng latLng) {
        LatLng intersectionPoint = mMapUtil.checkLatLngFallsWithinBoundsOfIntersection(latLng, mMapData.getIntersectionOfStreets());
        if (intersectionPoint != null && mLastIntersectionPoint != intersectionPoint) {
            mLastIntersectionPoint = intersectionPoint;
            addHapticAndVoiceFeedback(-1, 100, "Intersection at " + mLastChosenStreetName);
        } else if (mLastIntersectionPoint != null) {
            // when the point does not lie within the area of intersection,
            // check the distance between the current point and the last intersection point
            // when the distance is above the threshold then reset the intersection point to null.
            Point p1 = mMapUtil.convertLatlngToPoint(latLng);
            Point p2 = mMapUtil.convertLatlngToPoint(mLastIntersectionPoint);
            Log.d(TAG, "Distance: " + CommonUtil.distanceBetweenTwoPoints(p1, p2));
            if (CommonUtil.distanceBetweenTwoPoints(p1, p2) > 150) {
                mLastIntersectionPoint = null;
            }
        }
    }

    @Override
    public void swipeLeft() {
        if (mStreetsCoordinates == null) {
            return;
        }

        mLoggerUtil.log("Swiped Left");
        if (mIteratorIndexOfStreet == 0) {
            mIteratorIndexOfStreet = mStreetsCoordinates.size() - 1;
        } else {
            mIteratorIndexOfStreet--;
        }
        addHapticAndVoiceFeedback(SoundEffectConstants.NAVIGATION_LEFT, 30, mStreetNames.get(mIteratorIndexOfStreet));
    }

    @Override
    public void swipeRight() {
        if (mStreetsCoordinates == null) {
            return;
        }

        mLoggerUtil.log("Swiped Right");
        mIteratorIndexOfStreet++;
        mIteratorIndexOfStreet = (mIteratorIndexOfStreet) % mStreetsCoordinates.size();
        addHapticAndVoiceFeedback(SoundEffectConstants.NAVIGATION_RIGHT, 30, mStreetNames.get(mIteratorIndexOfStreet));
    }

    private void addHapticAndVoiceFeedback(int soundConstant, int milliseconds, String streetName) {
        if (soundConstant >= 0) {
            mOriginalContentView.playSoundEffect(soundConstant);
        }
        mMapUtil.addVoiceFeedback(streetName);
        mMapUtil.addHapticFeedback(milliseconds);
    }

    @Override
    public void enableSwipeGesture() {
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
    }

    @Override
    public void disableSwipeGesture() {
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(false);
    }

    @Override
    public void onDoubleTap() {
        mMap.getUiSettings().setZoomGesturesEnabled(false);
        CommonUtil.Toast(getContext(), "Double Tap");
    }

    @Override
    public void touchExplore(Point point) {
        //convert point to latlng
        LatLng latLng = mMapUtil.convertPointToLatlng(point);
        checkLatlngFallsWithinStreetArea(latLng, "explore");
        mLoggerUtil.log("Touch and Explore. Coordinates(lat,lng): (" + latLng.latitude + "," + latLng.longitude + ")");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapUtil.releaseUtils();
        mNetworkTask.cancel(true);
        mLoggerUtil.log("Experiment done!");
        mLoggerUtil.close();
    }

    public void setExtras(Bundle bundle) {
        mExtras = bundle;
    }
}
