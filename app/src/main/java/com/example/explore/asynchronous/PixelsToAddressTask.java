package com.example.explore.asynchronous;

import android.os.AsyncTask;
import android.util.Log;

import com.example.explore.CV;
import com.google.android.gms.maps.Projection;

import java.io.IOException;

public class PixelsToAddressTask extends AsyncTask<Void, Void, String> {

    private static final String TAG = PixelsToAddressTask.class.getSimpleName();

    private CV mCV;
    private Projection mProjection;
    private ITaskUpdate mTaskUpdate;

    public PixelsToAddressTask(CV cv, Projection projection, ITaskUpdate taskUpdate) {
        this.mCV = cv;
        this.mProjection = projection;
        this.mTaskUpdate = taskUpdate;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mTaskUpdate.processStarted();
    }

    @Override
    protected String doInBackground(Void... voids) {
        try {
            mCV.convertPixelToAddress(mProjection);
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());
        }
        return "Done";
    }

    @Override
    protected void onPostExecute(String s) {
        // once the process is done. we provide audio feedback to the user.
        mTaskUpdate.processCompleted();
    }


    public interface ITaskUpdate {
        void processStarted();

        void processCompleted();
    }

}
