package com.example.explore.Task;

import android.os.AsyncTask;
import android.os.Handler;

import com.example.explore.Handler.ITaskUpdate;
import com.example.explore.model.MapData;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class NetworkTask extends AsyncTask<String, Void, MapData> {

    public static final String REQUEST_METHOD = "GET";
    public static final String BASE_URL = "http://overpass-api.de/api/interpreter?data=";

    private ITaskUpdate mTaskUpdate;

    public NetworkTask(ITaskUpdate iTaskUpdate) {
        mTaskUpdate = iTaskUpdate;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTaskUpdate.processStarted();
            }
        }, 2000);
    }

    @Override
    protected MapData doInBackground(String... params) {
        MapData result = null;

        try {
            URL url = new URL(BASE_URL + params[0]);

            //Create a connection
            HttpURLConnection connection = (HttpURLConnection)
                    url.openConnection();
            //Set methods and timeouts
            connection.setRequestMethod(REQUEST_METHOD);

            //Connect to our url
            connection.connect();

            //Create a new InputStreamReader
            InputStreamReader streamReader = new
                    InputStreamReader(connection.getInputStream());
            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();
            String inputLine;
            //Check if the line we are reading is not null
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }

            String r = stringBuilder.toString();

            result = new Gson().fromJson(stringBuilder.toString(), MapData.class);

            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(final MapData result) {
        super.onPostExecute(result);

        if (isCancelled()) {
            return;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTaskUpdate.processCompleted(result);
            }
        }, 3000);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
