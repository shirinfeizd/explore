package com.example.explore.Handler;

import android.graphics.Bitmap;

public interface IUpdateMapHandler {
    void updateBitmap(Bitmap bitmap);
}