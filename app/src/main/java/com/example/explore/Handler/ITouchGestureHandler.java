package com.example.explore.Handler;

import android.graphics.Point;

public interface ITouchGestureHandler {
    void swipeLeft();

    void swipeRight();

    void enableSwipeGesture();

    void disableSwipeGesture();

    void onDoubleTap();

    void touchExplore(Point point);
}
