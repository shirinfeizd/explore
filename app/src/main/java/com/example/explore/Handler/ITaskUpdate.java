package com.example.explore.Handler;

import com.example.explore.model.MapData;

public interface ITaskUpdate {
    void processStarted();

    void processCompleted(MapData mapData);
}
