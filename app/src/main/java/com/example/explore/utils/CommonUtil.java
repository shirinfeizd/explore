package com.example.explore.utils;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class CommonUtil {
    private static final DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd hh:mm:ss", Locale.ENGLISH);

    public static String getDate() {
        return dateFormat.format(System.currentTimeMillis());
    }

    public static void Toast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static int distanceBetweenTwoPoints(Point p1, Point p2) {
        Log.d("Distance", "Point p1: (x,y) " + p1.x + " " + p1.y + " Point p2: (x,y) " + p2.x + " " + p2.y);
        return Math.abs(p1.x - p2.x) + Math.abs(p1.y - p2.y);
    }
}
