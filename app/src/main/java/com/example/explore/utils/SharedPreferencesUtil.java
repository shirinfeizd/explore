package com.example.explore.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Shared Preference for the application. We can store key value pairs.
 */
public class SharedPreferencesUtil {
    private static SharedPreferencesUtil mSharedPreferencesUtil;
    private SharedPreferences mSharedPreferences;

    private SharedPreferencesUtil(Context context) {
        mSharedPreferences = context.getSharedPreferences(
                "com.example.explore.utils", Context.MODE_PRIVATE);
    }

    public static synchronized SharedPreferencesUtil getInstance(Context context) {
        if (mSharedPreferencesUtil == null) {
            mSharedPreferencesUtil = new SharedPreferencesUtil(context);
        }

        return mSharedPreferencesUtil;
    }

    public void putLong(String key, long value) {
        mSharedPreferences.edit().putLong(key, value).apply();
    }

    public long getLong(String key) {
        long l = mSharedPreferences.getLong(key, 0);
        putLong(key, l + 1);
        return l + 1;
    }
}
