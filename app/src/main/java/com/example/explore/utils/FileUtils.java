package com.example.explore.utils;

import android.content.Context;

import java.io.File;
import java.io.IOException;

public class FileUtils {

    private static String mRootDir;
    private static String mUserDir;

    public static boolean createDir(String dirName) {
        File dir = new File(dirName);
        if (!dir.exists()) {
            return dir.mkdirs();
        }

        return true;
    }

    public static boolean createFile(String fileName) {
        File file = new File(fileName);

        if (!file.exists()) {
            try {
                return file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    public static File getExternalFileDir(Context context) {
        return context.getExternalFilesDir(null);
    }

    public static void createRootDir(Context context) {
        mRootDir = getExternalFileDir(context) + File.separator + "Logs";
        createDir(mRootDir);
    }

    public static void createUserDir(String userId) {
        mUserDir = mRootDir + File.separator + userId;
        createDir(mUserDir);
    }

    public static String getUserDir() {
        return mUserDir;
    }
}
