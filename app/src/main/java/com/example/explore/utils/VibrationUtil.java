package com.example.explore.utils;

import android.content.Context;
import android.os.Vibrator;
import android.widget.Toast;

public class VibrationUtil {
    private Context mContext;
    private Vibrator mVibrator;

    public VibrationUtil(Context context) {
        mContext = context;
        mVibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
    }

    /**
     * Adds vibration feedback.
     */
    public void addVibration(int millisec) {
        mVibrator.vibrate(millisec);
    }
}
