package com.example.explore.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.Display;

import com.example.explore.R;
import com.example.explore.model.Element;
import com.example.explore.model.Geometry;
import com.example.explore.model.MapData;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Utility class that contains all the helper functions related to map and its data(MapData).
 */
public class MapUtil {

    private static final String TAG = MapUtil.class.getSimpleName();
    private static final Set<String> STREET_TAGS = new HashSet<>(Arrays.asList("motorway", "trunk", "service", "primary", "secondary", "tertiary", "residential", "unclassified"));
    private static final Set<String> POI_TAGS = new HashSet<>(Arrays.asList("restaurant", "clothes", "gift", "watches", "fast_food", "dessert"));
    private VibrationUtil mVibrationUtil;
    private TTSUtil mTts;
    private Context mContext;
    private Projection mProjection;
    //we need this map because when we convert a point to latlng the precision is differing from the actual latlng present in the structure.
    private Map<Point, LatLng> poiPointLatLngMap = new HashMap<>();
    private List<Element> poiList = new ArrayList<>();

    public MapUtil(Context context) {
        this.mContext = context;
        mVibrationUtil = new VibrationUtil(context);
        mTts = new TTSUtil(context);
    }

    /**
     * Returns address for the given latlng.
     */
    public String getAddressFromLocation(LatLng latLng) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(mContext, Locale.getDefault());

        // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);

        Address address = addresses.get(0);
        return address.toString();
    }

    /**
     * Returns street name for the given latlng.
     */
    public String getStreetNameFromLatLng(LatLng latLng) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(mContext, Locale.getDefault());

        addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        Address address = addresses.get(0);
        return address.getThoroughfare();
    }

    /**
     * Retrieves all the Street and POIs from the entire map data.
     *
     * @param mapData data structure which contains the entire map data.
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void populateCategoricalDataFromMap(MapData mapData) {
        List<Element> streetList = new ArrayList<>();
        List<Element> poiList = new ArrayList<>();

        if (mapData == null || mapData.getElementList() == null) {
            return;
        }

        for (Element element : mapData.getElementList()) {
            if (element.getTag() == null) {
                continue;
            }

            String highwayTag = element.getTag().getHighway();
            if (highwayTag != null
                    && STREET_TAGS.contains(highwayTag)
                    && element.getTag().getName() != null) {
                streetList.add(element);
            }

            if (!element.getType().equals("node")) {
                continue;
            }

            String amenityTag = element.getTag().getAmenity();
            String shopTag = element.getTag().getShop();
            if (amenityTag != null && POI_TAGS.contains(amenityTag) && !poiList.contains(element)) {
                poiList.add(element);
            } else if (shopTag != null && POI_TAGS.contains(shopTag) && !poiList.contains(element)) {
                poiList.add(element);
            }
        }

        this.poiList = poiList;

        mapData.setStreetList(streetList);
        mapData.setPoiList(poiList);

        for (Element element : poiList) {
            Log.d(TAG, "populateCategoricalDataFromMap: " + element.getLatitude() + " " + element.getLongitude() + " " + element.getTag().getName());
        }
    }

    /**
     * @param poiCoordinates the list of point of interests.
     * @param divider        integer which divides the screen into parts and helps us in sorting based on the divided groups.
     * @return sorted points.
     */
    public List<Point> sortPOIList(List<LatLng> poiCoordinates, int divider) {
        List<Point> pointList = getPOICoordinatesAsPoint(poiCoordinates);

        Display mdisp = ((Activity) mContext).getWindowManager().getDefaultDisplay();
        int maxX = mdisp.getWidth();
        int maxY = mdisp.getHeight();

        Log.d(TAG, "sortPOIList: MAX Y:" + maxY);
        int range = maxY / divider;

        //Integer[] -> index, startRange, endRange List<Point> -> Coordinates within the range
        //Map<Integer[], List<Point>> rangeCoordinates = new HashMap<>();

        //contains the sorted points
        List<Point> sortedPOIPoints = new ArrayList<>();
        //contains the all the range of the points
        List<Integer> rangeList = new ArrayList<>();

        int startRange = 0;
        int endRange = range;

        //This loop makes sure we cover all the ranges from 0 - maxHeight of the screen.
        while (startRange <= maxY) {
            List<Point> points = getPointsWithInRange(startRange, endRange, pointList);

            //sort based on the x parameter
            Collections.sort(points, new SortComparator());
            sortedPOIPoints.addAll(points);

            rangeList.add(startRange);
            rangeList.add(endRange);

            startRange = endRange;
            endRange = endRange + startRange;

        }

        return sortedPOIPoints;
    }

    private List<Point> getPointsWithInRange(int startRange, int endRange, List<Point> pointList) {
        List<Point> points = new ArrayList<>();

        for (Point point : pointList) {
            if (point.y >= startRange && point.y < endRange) {
                points.add(point);
            }
        }

        return points;
    }

    /**
     * @param poiList
     * @return only latlngs of the corresponding poi present in the list.
     */
    public List<LatLng> getPOICoordinates(List<Element> poiList) {
        List<LatLng> latLngList = new ArrayList<>();

        for (Element element : poiList) {
            latLngList.add(new LatLng(element.getLatitude(), element.getLongitude()));
        }

        return latLngList;
    }

    private List<Point> getPOICoordinatesAsPoint(List<LatLng> poiCoordinates) {
        List<Point> pointList = new ArrayList<>();
        for (LatLng latLng : poiCoordinates) {
            Point point = convertLatlngToPoint(latLng);
            pointList.add(point);
            poiPointLatLngMap.put(point, latLng);
            Log.d(TAG, "getPOICoordinatesAsPoint: POI: " + getPoiName(getPOICorrespondingToPoint(point, poiList)) + " latlng: " + latLng + " point: " + point);
        }

        return pointList;
    }

    /**
     * Retrieves the intersection points of streets.
     *
     * @param mapData data structure which contains the entire map data.
     */
    public void populateIntersectingPointsOfStreets(MapData mapData) {
        if (mapData == null) {
            return;
        }
        List<Element> streetList = mapData.getStreetList();
        Map<LatLng, Element[]> intersectionOfStreets = new HashMap<>();

        for (Element street : streetList) {
            for (Element otherStreet : streetList) {
                //get the street names
                String streetName = street.getTag().getName();
                String otherStreetName = otherStreet.getTag().getName();

                if (!street.equals(otherStreet) &&
                        !streetName.equals(otherStreetName)) {
                    List<Long> nodeList = street.getNodesList();
                    for (int i = 0; i < nodeList.size(); i++) {
                        Long node = nodeList.get(i);
                        if (otherStreet.getNodesList().contains(node)) {
                            Geometry geometry = street.getGeometryList().get(i);
                            LatLng intersectionPoint = new LatLng(geometry.getLatitude(), geometry.getLongitude());
                            intersectionOfStreets.put(intersectionPoint, new Element[]{street, otherStreet});
                            Log.d(TAG, node + " : " + streetName + " " + otherStreetName);
                        }
                    }
                }

            }
        }

        mapData.setIntersectionOfStreets(intersectionOfStreets);
    }

    /**
     * Returns all the coordinates for every street.
     *
     * @param mapData data structure which contains the entire map data.
     * @return map which contains a street name with the list of associated latlngs that represent the street.
     */
    public Map<String, List<List<LatLng>>> getStreetCoordinates(MapData mapData) {
        if (mapData == null) {
            return null;
        }
        // a street can be split into multiple parts. hence we need list of list of streets.
        Map<String, List<List<LatLng>>> streetCoordinates = new HashMap<>();
        List<Element> streets = mapData.getStreetList();

        for (Element street : streets) {
            String streetName = street.getTag().getName();
            List<LatLng> coordinates = new ArrayList<>();
            for (Geometry geometry : street.getGeometryList()) {
                coordinates.add(new LatLng(geometry.getLatitude(), geometry.getLongitude()));
            }

            List<List<LatLng>> streetPartsMap = new ArrayList<>();
            if (streetCoordinates.get(streetName) != null) {
                streetPartsMap = streetCoordinates.get(streetName);
            }
            streetPartsMap.add(coordinates);
            streetCoordinates.put(streetName, streetPartsMap);
        }
        return streetCoordinates;
    }

    private float getDistanceFromCenter(LatLng center, LatLng point) {
        double cX = center.latitude;
        double cY = center.longitude;
        double pX = point.latitude;
        double pY = point.longitude;

        float[] results = new float[1];

        Location.distanceBetween(cX, cY, pX, pY, results);

        return results[0];
    }

    /**
     * Returns poi if the user clicked latlng falls within the bounds of it.
     *
     * @param point   latlng associated with the user click.
     * @param poiList list of point of interest
     * @return poi if the user clicked latlng is within in bounds otherwise null
     */
    public Element checkLatLngFallsWithinBoundsOfPOI(LatLng point, List<Element> poiList) {
        if (poiList == null) {
            return null;
        }

        for (Element element : poiList) {
            LatLng center = new LatLng(element.getLatitude(), element.getLongitude());
            if (isWithInBoundFromCenter(center, point, 15)) {
                return element;
            }
        }

        return null;
    }

    /**
     * @param point         latlng associated with the user click.
     * @param intersections the intersection points associated with the street list.
     * @return intersection point if the user clicked point falls within any of the intersection points.
     */
    public LatLng checkLatLngFallsWithinBoundsOfIntersection(LatLng point, Map<LatLng, Element[]> intersections) {
        for (LatLng intersectionPoint : intersections.keySet()) {
            if (isWithInBoundFromCenter(intersectionPoint, point, 10)) {
                return intersectionPoint;
            }
        }

        return null;
    }

    private boolean isWithInBoundFromCenter(LatLng center, LatLng point, int bound) {
        float dist = getDistanceFromCenter(center, point);
        return dist < bound;
    }

    public String getPoiName(Element element) {
        String nameTag = element.getTag().getName();
        return nameTag == null ? element.getTag().getBrand() : nameTag;
    }

    /**
     * @param point              coordinate(latitude,longitude)
     * @param streetsCoordinates
     * @return name of the street if the point falls into any of the street bounds.
     */
    public String checkLatLngFallsWithinBoundsOfAStreet(LatLng point, Map<String, List<List<LatLng>>> streetsCoordinates) {
        if (streetsCoordinates == null) {
            return null;
        }

        for (String streetName : streetsCoordinates.keySet()) {
            List<List<LatLng>> streetParts = streetsCoordinates.get(streetName);
            if (streetParts == null) {
                return null;
            }
            for (List<LatLng> part : streetParts) {
                boolean contain = PolyUtil.isLocationOnPath(point, part, true, 10);
                if (contain) {
                    return streetName;
                }
            }
        }

        return null;
    }

    public void addHapticFeedback(int millisec) {
        mVibrationUtil.addVibration(millisec);
    }

    /**
     * @param name could be a process update or name of the POI/Street
     */
    public void addVoiceFeedback(String name) {
        if (name != null) {
            mTts.speak(name, null);
        }
    }

    public void setProjection(Projection projection) {
        mProjection = projection;
    }

    /**
     * SortComparator sorts the POIs based on the x distance as they are grouped based on the y range.
     */
    private class SortComparator implements Comparator<Point> {
        @Override
        public int compare(Point o1, Point o2) {
            return o1.x - o2.x;
        }
    }

    /**
     * Release the tts utility when the screen is destroyed otherwise we will get activity leak exception.
     */
    public void releaseUtils() {
        mTts.release();
    }

    /**
     * @param latLng coordinate(latitude, longitude)
     * @return point corresponding to the coordinate
     */
    public Point convertLatlngToPoint(LatLng latLng) {
        return mProjection.toScreenLocation(latLng);
    }

    public LatLng convertPointToLatlng(Point point) {
        LatLng latLng = mProjection.fromScreenLocation(point);
        Log.d(TAG, latLng.latitude + " " + latLng.longitude);
        return latLng;
    }

    /**
     * @param point   the point based on the swipe(left/right).
     * @param poiList poiList containing all the elements of the POIs.
     * @return poi element corresponding to the point.
     */
    public Element getPOICorrespondingToPoint(Point point, List<Element> poiList) {
        LatLng latLng = poiPointLatLngMap.get(point);

        for (Element element : poiList) {
            if (element.getLatitude() == latLng.latitude && element.getLongitude() == latLng.longitude) {
                return element;
            }
        }

        return null;
    }

    /**
     * @param type the type of the POI.
     * @return the drawable resource corresponding to the type.
     */
    public int getIconBasedOnType(String type) {
        int res = -1;

        switch (type) {
            case "restaurant":
            case "fast_food":
            case "dessert":
                res = R.drawable.restaurant;
                break;
            case "clothes":
            case "gift":
            case "watches":
                res = R.drawable.shopping;
                break;
            default:
                break;
        }

        return res;
    }
}
