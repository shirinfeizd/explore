package com.example.explore.utils;

import android.content.Context;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * LoggerUtil for keeping a log of all the user interactions.
 */
public class LoggerUtil {
    private BufferedWriter mBufferWriter;

    public LoggerUtil(Context context, String experiment, String userId) {
        // get the no of times this experiment has been logged.
        // we create a separate log file if the same experiment is done twice.
        long experimentCount = SharedPreferencesUtil.getInstance(context).getLong(userId + "_" + experiment);
        String expFile = FileUtils.getUserDir() + File.separator + experiment + "_" + experimentCount + ".txt";

        if (FileUtils.createFile(expFile)) {
            try {
                mBufferWriter = new BufferedWriter(new FileWriter(expFile, true));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param msg the message to be logged.
     */
    public void log(String msg) {
        try {
            mBufferWriter.append(getLog(msg));
            mBufferWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            mBufferWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getLog(String msg) {
        return CommonUtil.getDate() + " " + msg;
    }

}