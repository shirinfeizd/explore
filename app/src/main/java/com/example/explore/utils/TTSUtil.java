package com.example.explore.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.example.explore.R;

import java.util.HashMap;
import java.util.Locale;

@SuppressLint("NewApi")
public class TTSUtil implements TextToSpeech.OnInitListener {

    private static final String TAG = TTSUtil.class.getSimpleName();
    private static final int QUEUE_TYPE = TextToSpeech.QUEUE_FLUSH;

    private TTSUtil mUtility;
    private TextToSpeech mTts;
    private Context mContext;

    public TTSUtil(Context context) {
        mContext = context;
        mTts = new TextToSpeech(context, this);
    }

    @Override
    public void onInit(int status) {
        if (status == android.speech.tts.TextToSpeech.SUCCESS) {
            int result = mTts.setLanguage(Locale.US);
            if (result == android.speech.tts.TextToSpeech.LANG_MISSING_DATA
                    || result == android.speech.tts.TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e(TAG, "TTS This Language is not supported");
            } else {
//                speak(mContext.getResources().getString(R.string.hello), null);
            }
        } else {
            Log.e(TAG, "TTS Initialization Failed!");
        }
    }

    /**
     * This function provides the speech feedback for the given message.
     */
    public void speak(String msg, HashMap<String, String> params) {
        mTts.speak(msg, QUEUE_TYPE, params);
    }

    public void release() {
        mTts.stop();
        mTts.shutdown();
    }
}
