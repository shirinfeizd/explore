package com.example.explore;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.example.explore.utils.MapUtil;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;

import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class CV {
    private static final String TAG = CV.class.getName();
    private Mat mat;
    private Context context;
    private MapUtil mMapUtil;

    public CV(Context context) {
        mat = new Mat();
        this.context = context;
        mMapUtil = new MapUtil(context);
    }

    public Bitmap getBitmap() {
        return convertMatToBitMap(mat);
    }

    /**
     * Bitmap is converted to mat.
     */
    public void setBitmap(Bitmap bmap, float zoomLevel) {
        Bitmap bmp32 = bmap.copy(Bitmap.Config.ARGB_8888, true);
        Utils.bitmapToMat(bmp32, mat);
    }

    private Bitmap convertMatToBitMap(Mat input) {
        Bitmap bmp = null;
        Mat rgb = new Mat();
        Imgproc.cvtColor(input, rgb, Imgproc.COLOR_BGR2RGB);

        try {
            bmp = Bitmap.createBitmap(rgb.cols(), rgb.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(rgb, bmp);
        } catch (CvException e) {
            Log.d("Exception", e.getMessage());
        }
        return bmp;
    }

    public void drawCircleOnPixel(int x, int y) {
        Imgproc.circle(
                mat,                           //Matrix obj of the image
                new Point(x, y),               //Center of the circle
                15,                     //Radius
                new Scalar(0, 0, 255),         //Scalar object for color
                5                    //Thickness of the circle
        );
    }

    /**
     * Converts every pixel in the mat object into an address.
     * Writes the address(s) to the external file.
     */
    @RequiresApi(api = Build.VERSION_CODES.FROYO)
    public void convertPixelToAddress(Projection projection) throws IOException {
        String fileName = "AddressList.txt";
        File file = new File(context.getExternalFilesDir(null), fileName);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        OutputStreamWriter oswriter = new OutputStreamWriter(fileOutputStream);
        BufferedWriter bwriter = new BufferedWriter(oswriter);

        for (int i = 0; i < mat.rows(); i++) {
            for (int j = 0; j < mat.cols(); j++) {
                LatLng latLng = projection.fromScreenLocation(new android.graphics.Point(i, j));
                try {
                    String address = mMapUtil.getAddressFromLocation(latLng);
                    bwriter.append(address);
                    bwriter.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        bwriter.close();
        oswriter.close();
        fileOutputStream.close();
        Log.d(TAG, "Conversion Completed!");
    }
}