package com.example.explore.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.example.explore.R;
import com.example.explore.fragments.AreaFragment;

public class Area extends AppCompatActivity {

    private ImageView mSnapIV;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area);
        mSnapIV = findViewById(R.id.snapOfMapIV);

        AreaFragment areaFragment = (AreaFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        Bundle bundle = this.getIntent().getExtras();

        if (areaFragment != null) {
            areaFragment.setExtras(bundle);
        }
    }

    public void updateImageView(Bitmap bitmap) {
        mSnapIV.setImageBitmap(bitmap);
    }
}
