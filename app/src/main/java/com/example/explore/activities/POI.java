package com.example.explore.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.example.explore.R;
import com.example.explore.fragments.POIFragment;
import com.example.explore.utils.MapUtil;

public class POI extends AppCompatActivity {

    private ProgressBar mProgressBar;
    private MapUtil mMapUtil;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_poi);
        mProgressBar = findViewById(R.id.progress_circular);

        mMapUtil = new MapUtil(this);
        mMapUtil.addVoiceFeedback(getString(R.string.welcome_poi_screen));

        POIFragment poiFragment = (POIFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        Bundle bundle = this.getIntent().getExtras();

        if (poiFragment != null) {
            poiFragment.setExtras(bundle);
        }
    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapUtil.releaseUtils();
    }
}
