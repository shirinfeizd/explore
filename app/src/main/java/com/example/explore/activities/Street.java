package com.example.explore.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.example.explore.R;
import com.example.explore.fragments.StreetFragment;

public class Street extends AppCompatActivity {
    private ProgressBar mProgressBar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_street);
        mProgressBar = findViewById(R.id.progress_circular);

        StreetFragment streetFragment = (StreetFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        Bundle bundle = this.getIntent().getExtras();

        if(streetFragment != null) {
            streetFragment.setExtras(bundle);
        }
    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
