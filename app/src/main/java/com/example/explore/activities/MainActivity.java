package com.example.explore.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.explore.R;
import com.example.explore.utils.FileUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener, AdapterView.OnItemSelectedListener {

    private static final String CHOOSE_EXPERIMENT = "Choose Experiment";

    private EditText mUserId;
    private Spinner mOrderOneSpinner;
    private Spinner mOrderTwoSpinner;
    private Spinner mOrderThreeSpinner;
    private Spinner mOrderFourSpinner;
    private Spinner mOrderFiveSpinner;
    private Spinner mOrderSixSpinner;
    private Spinner mOrderSevenSpinner;
    private Spinner mOrderEightSpinner;
    private Spinner mOrderNineSpinner;

    private Button mSubmitButton;
    private ArrayAdapter<String> mOrderOneSpinnerAdapter;
    private ArrayAdapter<String> mOrderTwoSpinnerAdapter;
    private ArrayAdapter<String> mOrderThreeSpinnerAdapter;
    private ArrayAdapter<String> mOrderFourSpinnerAdapter;
    private ArrayAdapter<String> mOrderFiveSpinnerAdapter;
    private ArrayAdapter<String> mOrderSixSpinnerAdapter;
    private ArrayAdapter<String> mOrderSevenSpinnerAdapter;
    private ArrayAdapter<String> mOrderEightSpinnerAdapter;
    private ArrayAdapter<String> mOrderNineSpinnerAdapter;

    private HashMap<Integer, String> mSelectedItemMap = new HashMap<>();
    List<String> mExperimentsList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mUserId = findViewById(R.id.user_id_et);
        mUserId.setText("1");
        mOrderOneSpinner = findViewById(R.id.order_1_sp);
        mOrderTwoSpinner = findViewById(R.id.order_2_sp);
        mOrderThreeSpinner = findViewById(R.id.order_3_sp);
        mOrderFourSpinner = findViewById(R.id.order_4_sp);
        mOrderFiveSpinner = findViewById(R.id.order_5_sp);
        mOrderSixSpinner = findViewById(R.id.order_6_sp);
        mOrderSevenSpinner = findViewById(R.id.order_7_sp);
        mOrderEightSpinner = findViewById(R.id.order_8_sp);
        mOrderNineSpinner = findViewById(R.id.order_9_sp);

        mSubmitButton = findViewById(R.id.submit_button);

        populateSpinnerValues();
        addListeners();

//        Intent intent = new Intent(MainActivity.this, POI.class);
//        startActivity(intent);
    }

    private void populateSpinnerValues() {
        mExperimentsList = new LinkedList<>(Arrays.asList(getResources().getStringArray(R.array.experiments)));

        mOrderOneSpinnerAdapter = setAdapterForSpinner(mOrderOneSpinner);
        mOrderTwoSpinnerAdapter = setAdapterForSpinner(mOrderTwoSpinner);
        mOrderThreeSpinnerAdapter = setAdapterForSpinner(mOrderThreeSpinner);
        mOrderFourSpinnerAdapter = setAdapterForSpinner(mOrderFourSpinner);
        mOrderFiveSpinnerAdapter = setAdapterForSpinner(mOrderFiveSpinner);
        mOrderSixSpinnerAdapter = setAdapterForSpinner(mOrderSixSpinner);
        mOrderSevenSpinnerAdapter = setAdapterForSpinner(mOrderSevenSpinner);
        mOrderEightSpinnerAdapter = setAdapterForSpinner(mOrderEightSpinner);
        mOrderNineSpinnerAdapter = setAdapterForSpinner(mOrderNineSpinner);
    }

    private ArrayAdapter<String> setAdapterForSpinner(Spinner spinner) {
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, new LinkedList<>(mExperimentsList));
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);

        return spinnerAdapter;
    }

    private void addListeners() {
        mOrderOneSpinner.setOnItemSelectedListener(this);
        mOrderTwoSpinner.setOnItemSelectedListener(this);
        mOrderThreeSpinner.setOnItemSelectedListener(this);
        mOrderFourSpinner.setOnItemSelectedListener(this);
        mOrderFiveSpinner.setOnItemSelectedListener(this);
        mOrderSixSpinner.setOnItemSelectedListener(this);
        mOrderSevenSpinner.setOnItemSelectedListener(this);
        mOrderEightSpinner.setOnItemSelectedListener(this);
        mOrderNineSpinner.setOnItemSelectedListener(this);

        mOrderOneSpinner.setOnTouchListener(this);
        mOrderTwoSpinner.setOnTouchListener(this);
        mOrderThreeSpinner.setOnTouchListener(this);
        mOrderFourSpinner.setOnTouchListener(this);
        mOrderFiveSpinner.setOnTouchListener(this);
        mOrderSixSpinner.setOnTouchListener(this);
        mOrderSevenSpinner.setOnTouchListener(this);
        mOrderEightSpinner.setOnTouchListener(this);
        mOrderNineSpinner.setOnTouchListener(this);

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUserId.getText().toString().isEmpty()
                        || mOrderOneSpinner.getSelectedItem().toString().equals(CHOOSE_EXPERIMENT)
                        || mOrderTwoSpinner.getSelectedItem().toString().equals(CHOOSE_EXPERIMENT)) {
                    Toast.makeText(MainActivity.this, "Please fill all the required fields", Toast.LENGTH_LONG).show();
                } else {
                    HashMap<Integer, String> map = new HashMap<>();
                    map.put(1, mOrderOneSpinner.getSelectedItem().toString());
                    map.put(2, mOrderTwoSpinner.getSelectedItem().toString());

                    FileUtils.createUserDir(mUserId.getText().toString());

                    Intent intent = new Intent(MainActivity.this, OrderExperimentActivity.class);
                    intent.putExtra("Order", mSelectedItemMap);
                    intent.putExtra("UserId", mUserId.getText().toString());
                    startActivity(intent);

                    finish();
                }
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {

            case R.id.order_1_sp:
                String selectedItem1 = mOrderOneSpinner.getSelectedItem().toString();
                List<String> expListSp1 = getExperiments(1, selectedItem1);
                mOrderOneSpinnerAdapter = new ArrayAdapter<>(this,
                        android.R.layout.simple_spinner_item, expListSp1);
                mOrderOneSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mOrderOneSpinner.setAdapter(mOrderOneSpinnerAdapter);
                mOrderOneSpinner.setSelection(getPosFromList(selectedItem1, expListSp1));
                break;
            case R.id.order_2_sp:
                String selectedItem2 = mOrderTwoSpinner.getSelectedItem().toString();
                List<String> expListSp2 = getExperiments(2, selectedItem2);
                mOrderTwoSpinnerAdapter = new ArrayAdapter<>(this,
                        android.R.layout.simple_spinner_item, expListSp2);
                mOrderTwoSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mOrderTwoSpinner.setAdapter(mOrderTwoSpinnerAdapter);
                mOrderTwoSpinner.setSelection(getPosFromList(selectedItem2, expListSp2));
                break;
            case R.id.order_3_sp:
                String selectedItem3 = mOrderThreeSpinner.getSelectedItem().toString();
                List<String> expListSp3 = getExperiments(3, selectedItem3);
                mOrderThreeSpinnerAdapter = new ArrayAdapter<>(this,
                        android.R.layout.simple_spinner_item, expListSp3);
                mOrderThreeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mOrderThreeSpinner.setAdapter(mOrderThreeSpinnerAdapter);
                mOrderThreeSpinner.setSelection(getPosFromList(selectedItem3, expListSp3));
                break;
            case R.id.order_4_sp:
                String selectedItem4 = mOrderFourSpinner.getSelectedItem().toString();
                List<String> expListSp4 = getExperiments(4, selectedItem4);
                mOrderFourSpinnerAdapter = new ArrayAdapter<>(this,
                        android.R.layout.simple_spinner_item, expListSp4);
                mOrderFourSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mOrderFourSpinner.setAdapter(mOrderFourSpinnerAdapter);
                mOrderFourSpinner.setSelection(getPosFromList(selectedItem4, expListSp4));
                break;

            case R.id.order_5_sp:
                String selectedItem5 = mOrderFiveSpinner.getSelectedItem().toString();
                List<String> expListSp5 = getExperiments(5, selectedItem5);
                mOrderFiveSpinnerAdapter = new ArrayAdapter<>(this,
                        android.R.layout.simple_spinner_item, expListSp5);
                mOrderFiveSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mOrderFiveSpinner.setAdapter(mOrderFiveSpinnerAdapter);
                mOrderFiveSpinner.setSelection(getPosFromList(selectedItem5, expListSp5));
                break;
            case R.id.order_6_sp:
                String selectedItem6 = mOrderSixSpinner.getSelectedItem().toString();
                List<String> expListSp6 = getExperiments(6, selectedItem6);
                mOrderSixSpinnerAdapter = new ArrayAdapter<>(this,
                        android.R.layout.simple_spinner_item, expListSp6);
                mOrderSixSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mOrderSixSpinner.setAdapter(mOrderSixSpinnerAdapter);
                mOrderSixSpinner.setSelection(getPosFromList(selectedItem6, expListSp6));
                break;
            case R.id.order_7_sp:
                String selectedItem7 = mOrderSevenSpinner.getSelectedItem().toString();
                List<String> expListSp7 = getExperiments(7, selectedItem7);
                mOrderSevenSpinnerAdapter = new ArrayAdapter<>(this,
                        android.R.layout.simple_spinner_item, expListSp7);
                mOrderSevenSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mOrderSevenSpinner.setAdapter(mOrderSevenSpinnerAdapter);
                mOrderSevenSpinner.setSelection(getPosFromList(selectedItem7, expListSp7));
                break;
            case R.id.order_8_sp:
                String selectedItem8 = mOrderEightSpinner.getSelectedItem().toString();
                List<String> expListSp8 = getExperiments(8, selectedItem8);
                mOrderEightSpinnerAdapter = new ArrayAdapter<>(this,
                        android.R.layout.simple_spinner_item, expListSp8);
                mOrderEightSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mOrderEightSpinner.setAdapter(mOrderEightSpinnerAdapter);
                mOrderEightSpinner.setSelection(getPosFromList(selectedItem8, expListSp8));
                break;
            case R.id.order_9_sp:
                String selectedItem9 = mOrderNineSpinner.getSelectedItem().toString();
                List<String> expListSp9 = getExperiments(9, selectedItem9);
                mOrderNineSpinnerAdapter = new ArrayAdapter<>(this,
                        android.R.layout.simple_spinner_item, expListSp9);
                mOrderNineSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mOrderNineSpinner.setAdapter(mOrderNineSpinnerAdapter);
                mOrderNineSpinner.setSelection(getPosFromList(selectedItem9, expListSp9));
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + v.getId());
        }
        return false;
    }

    private List<String> getExperiments(int spinnerNo, String selectedExperiment) {
        List<String> newList = new LinkedList<>(mExperimentsList);

        for (Integer key : mSelectedItemMap.keySet()) {
            if (!key.equals(spinnerNo)) {
                newList.remove(mSelectedItemMap.get(key));
            }
        }

        if (!selectedExperiment.equals(CHOOSE_EXPERIMENT)) {
            newList.remove(0);
        }

        return newList;
    }

    private int getPosFromList(String selectedExperiment, List<String> experimentsList) {
        int i = 0;

        for (String experiment : experimentsList) {
            if (experiment.equals(selectedExperiment)) {
                return i;
            }
            i++;
        }

        return -1;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedExperiment = null;
        int key = -1;
        switch (parent.getId()) {
            case R.id.order_1_sp:
                mOrderOneSpinner.setSelection(1);
                selectedExperiment = mOrderOneSpinnerAdapter.getItem(1);
                key = 1;
                break;
            case R.id.order_2_sp:
                mOrderTwoSpinner.setSelection(2);
                selectedExperiment = mOrderTwoSpinnerAdapter.getItem(2);
                key = 2;
                break;
            case R.id.order_3_sp:
                mOrderThreeSpinner.setSelection(3);
                selectedExperiment = mOrderThreeSpinnerAdapter.getItem(3);
                key = 3;
                break;
            case R.id.order_4_sp:
                mOrderFourSpinner.setSelection(4);
                selectedExperiment = mOrderFourSpinnerAdapter.getItem(4);
                key = 4;
                break;
            case R.id.order_5_sp:
                mOrderFiveSpinner.setSelection(5);
                selectedExperiment = mOrderFiveSpinnerAdapter.getItem(5);
                key = 5;
                break;
            case R.id.order_6_sp:
                mOrderSixSpinner.setSelection(6);
                selectedExperiment = mOrderSixSpinnerAdapter.getItem(6);
                key = 6;
                break;
            case R.id.order_7_sp:
                mOrderSevenSpinner.setSelection(7);
                selectedExperiment = mOrderSevenSpinnerAdapter.getItem(7);
                key = 7;
                break;
            case R.id.order_8_sp:
                mOrderEightSpinner.setSelection(8);
                selectedExperiment = mOrderEightSpinnerAdapter.getItem(position);
                key = 8;
                break;
            case R.id.order_9_sp:
                mOrderNineSpinner.setSelection(9);
                selectedExperiment = mOrderNineSpinnerAdapter.getItem(9);
                key = 9;
                break;

            default:
                break;
        }
        if (selectedExperiment != null && !selectedExperiment.equals(CHOOSE_EXPERIMENT)) {
            mSelectedItemMap.remove(key);
            mSelectedItemMap.put(key, selectedExperiment);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
