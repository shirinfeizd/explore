package com.example.explore.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.explore.R;
import com.example.explore.utils.MapUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderExperimentActivity extends AppCompatActivity implements View.OnClickListener {

    public static String LATITUDE = "latitude";
    public static String LONGITUDE = "longitude";
    public static String ZOOM_LEVEL = "zoom_level";
    public static String USER_ID = "user_id";
    public static String EXPERIMENT = "experiment";

    private Button mOrderOneButton;
    private Button mOrderTwoButton;
    private Button mOrderThreeButton;
    private Button mOrderFourButton;
    private Button mOrderFiveButton;
    private Button mOrderSixButton;
    private Button mOrderSevenButton;
    private Button mOrderEightButton;
    private Button mOrderNineButton;
    private List<Button> mOrderButtonSet = new ArrayList<>();
    private String mUserId;
    private MapUtil mMapUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_experiment);

        Intent intent = getIntent();
        HashMap<Integer, String> map = (HashMap<Integer, String>) intent.getSerializableExtra("Order");
        mUserId = intent.getExtras().getString("UserId");
        mMapUtil = new MapUtil(this);

        mOrderOneButton = findViewById(R.id.ord_1_button);
        mOrderTwoButton = findViewById(R.id.ord_2_button);
        mOrderThreeButton = findViewById(R.id.ord_3_button);
        mOrderFourButton = findViewById(R.id.ord_4_button);
        mOrderFiveButton = findViewById(R.id.ord_5_button);
        mOrderSixButton = findViewById(R.id.ord_6_button);
        mOrderSevenButton = findViewById(R.id.ord_7_button);
        mOrderEightButton = findViewById(R.id.ord_8_button);
        mOrderNineButton = findViewById(R.id.ord_9_button);

        mOrderButtonSet.add(mOrderOneButton);
        mOrderButtonSet.add(mOrderTwoButton);
        mOrderButtonSet.add(mOrderThreeButton);
        mOrderButtonSet.add(mOrderFourButton);
        mOrderButtonSet.add(mOrderFiveButton);
        mOrderButtonSet.add(mOrderSixButton);
        mOrderButtonSet.add(mOrderSevenButton);
        mOrderButtonSet.add(mOrderEightButton);
        mOrderButtonSet.add(mOrderNineButton);

        populateButtonOrders(map, mOrderButtonSet);
        populateClickListeners();
    }

    private void populateClickListeners() {

        for (Button button : mOrderButtonSet) {
            button.setOnClickListener(this);
        }
    }

    private void populateButtonOrders(HashMap<Integer, String> map, List<Button> orderButtonSet) {
        for (Integer orderNo : map.keySet()) {
            orderButtonSet.get(orderNo - 1).setText(map.get(orderNo));
        }
    }

    @Override
    public void onClick(View v) {
        Button button = null;
        switch (v.getId()) {
            case R.id.ord_1_button:
                button = mOrderButtonSet.get(0);
                break;
            case R.id.ord_2_button:
                button = mOrderButtonSet.get(1);
                break;
            case R.id.ord_3_button:
                button = mOrderButtonSet.get(2);
                break;
            case R.id.ord_4_button:
                button = mOrderButtonSet.get(3);
                break;
            case R.id.ord_5_button:
                button = mOrderButtonSet.get(4);
                break;
            case R.id.ord_6_button:
                button = mOrderButtonSet.get(5);
                break;
            case R.id.ord_7_button:
                button = mOrderButtonSet.get(6);
                break;
            case R.id.ord_8_button:
                button = mOrderButtonSet.get(7);
                break;
            case R.id.ord_9_button:
                button = mOrderButtonSet.get(8);
                break;
        }

        if (button != null) {
            startActivityForTheSelectedExperiment(button.getText().toString());
            button.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            button.setTextColor(getResources().getColor(R.color.white));
        }
    }

    private void startActivityForTheSelectedExperiment(String experiment) {
        Intent intent = getIntent(experiment);

        double latitude = 0;
        double longitude = 0;
        float zoomLevel = 0;

        switch (experiment) {
            case "POI 1":
            case "STREET 1":
            case "AREA 1":
                latitude = 40.722024;
                longitude = -74.002654;
                zoomLevel = 18.0f;
                break;

            case "POI 2":
            case "STREET 2":
            case "AREA 2":
                latitude = 40.804184;
                longitude = -73.965711;
                zoomLevel = 17.0f;
                break;

            case "POI 3":
            case "STREET 3":
            case "AREA 3":
                latitude = 40.797886;
                longitude = -73.940648;
                zoomLevel = 16.5f;
                break;
            default:
                Toast.makeText(this, "Class not implemented yet!", Toast.LENGTH_SHORT).show();
        }

        if (intent != null) {
            Bundle bundle = new Bundle();
            bundle.putString(USER_ID, mUserId);
            bundle.putString(EXPERIMENT, experiment);
            bundle.putDouble(LATITUDE, latitude);
            bundle.putDouble(LONGITUDE, longitude);
            bundle.putFloat(ZOOM_LEVEL, zoomLevel);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    private Intent getIntent(String type) {
        if (type == null) {
            return null;
        }

        if (type.contains("POI")) {
            mMapUtil.addVoiceFeedback(getString(R.string.welcome_poi_screen));
            return new Intent(OrderExperimentActivity.this, POI.class);
        } else if (type.contains("STREET")) {
            mMapUtil.addVoiceFeedback(getString(R.string.welcome_street_screen));
            return new Intent(OrderExperimentActivity.this, Street.class);
        } else {
            mMapUtil.addVoiceFeedback(getString(R.string.welcome_area_screen));
            return new Intent(OrderExperimentActivity.this, Area.class);
        }
    }
}
