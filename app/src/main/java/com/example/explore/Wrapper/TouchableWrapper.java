package com.example.explore.Wrapper;

import android.content.Context;
import android.graphics.Point;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.widget.FrameLayout;

import com.example.explore.Handler.ITouchGestureHandler;

import org.opencv.core.Mat;

public class TouchableWrapper extends FrameLayout implements GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener {
    private static final String DEBUG_TAG = "Gestures";

    private ITouchGestureHandler mITouchGestureHandler;
    private GestureDetectorCompat mDetector;
    private int mPointerCount = 0;
    private boolean mMultiTouch = false;
    private static final int SWIPE_THRESHOLD = 100;
    private static final int FLING_VELOCITY_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 500;
    private VelocityTracker mVelocityTracker = null;
    private boolean mFling = false;
    private boolean mExplore = false;

    public TouchableWrapper(Context context) {
        super(context);
    }

    public TouchableWrapper(Context context, ITouchGestureHandler iTouchGestureHandler) {
        super(context);
        mITouchGestureHandler = iTouchGestureHandler;
        mDetector = new GestureDetectorCompat(getContext(), this);
        mDetector.setOnDoubleTapListener(this);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        int index = event.getActionIndex();
        int pointerId = event.getPointerId(index);

        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                if (mVelocityTracker == null) {
                    // Retrieve a new VelocityTracker object to watch the
                    // velocity of a motion.
                    mVelocityTracker = VelocityTracker.obtain();
                } else {
                    // Reset the velocity tracker back to its initial state.
                    mVelocityTracker.clear();
                }

                Log.d(DEBUG_TAG, "dispatchTouchEvent: " + event.getAction());
                mPointerCount++;
                if (mPointerCount > 1) {
                    mITouchGestureHandler.enableSwipeGesture();
                    mMultiTouch = true;
                    break;
                }
                mMultiTouch = false;
                break;

            case MotionEvent.ACTION_MOVE:
                if (mFling) {
                    break;
                }

                mVelocityTracker.addMovement(event);
                mVelocityTracker.computeCurrentVelocity(1000);

                float velocityX = mVelocityTracker.getXVelocity(pointerId);
                float velocityY = mVelocityTracker.getYVelocity(pointerId);

                Log.d(DEBUG_TAG, "X velocity: " + mVelocityTracker.getXVelocity(pointerId));
                Log.d(DEBUG_TAG, "Y velocity: " + mVelocityTracker.getYVelocity(pointerId));

                if (!mExplore && (Math.abs(velocityX) > 700 || Math.abs(velocityY) > 700)) {
                    mFling = true;
                    break;
                }

                if (Math.abs(velocityX) < 1 && Math.abs(velocityY) < 1) {
                    break;
                }

                Log.d(DEBUG_TAG, "dispatchTouchEvent: " + event.getAction());
                //multi pointers can also result in long press but we should not activate the touch explore mode.
                if (!mMultiTouch) {
                    mExplore = true;
                    mITouchGestureHandler.touchExplore(new Point((int) event.getX(), (int) event.getY()));
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                mFling = false;
                mExplore = false;
                Log.d(DEBUG_TAG, "dispatchTouchEvent: " + event.getAction());
                mITouchGestureHandler.disableSwipeGesture();
                mPointerCount--;
                break;
        }


        if (!mMultiTouch) {
            this.mDetector.onTouchEvent(event);
        }

        return super.dispatchTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDown: " + event.toString());
        return true;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {
        Log.d(DEBUG_TAG, "onFling: " + event1.toString() + event2.toString());
        float diffY = event2.getY() - event1.getY();
        float diffX = event2.getX() - event1.getX();
        if (Math.abs(diffX) > Math.abs(diffY)) {
            //check whether a swipe has happened
            if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > FLING_VELOCITY_THRESHOLD) {
                //identify left or right swipe.
                if (diffX > 0) {
                    mITouchGestureHandler.swipeRight();
                } else {
                    mITouchGestureHandler.swipeLeft();
                }
            }
        }
        return true;
    }

    @Override
    public void onLongPress(MotionEvent event) {
        Log.d(DEBUG_TAG, "onLongPress: " + event.toString());
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX,
                            float distanceY) {
        Log.d(DEBUG_TAG, "onScroll: " + event1.toString() + event2.toString());
        return true;
    }

    @Override
    public void onShowPress(MotionEvent event) {
        Log.d(DEBUG_TAG, "onShowPress: " + event.toString());
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        Log.d(DEBUG_TAG, "onSingleTapUp: " + event.toString());
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDoubleTap: " + event.toString());
        mITouchGestureHandler.onDoubleTap();
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDoubleTapEvent: " + event.toString());
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
        Log.d(DEBUG_TAG, "onSingleTapConfirmed: " + event.toString());
        return true;
    }
}