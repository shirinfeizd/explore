package com.example.explore.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class MapData {
    @SerializedName("elements")
    List<Element> elementList;

    List<Element> streetList;
    List<Element> poiList;
    Map<LatLng, Element[]> intersectionOfStreets;

    public List<Element> getElementList() {
        return elementList;
    }

    public void setElementList(List<Element> elementList) {
        this.elementList = elementList;
    }

    public List<Element> getStreetList() {
        return streetList;
    }

    public void setStreetList(List<Element> streetList) {
        this.streetList = streetList;
    }

    public List<Element> getPoiList() {
        return poiList;
    }

    public void setPoiList(List<Element> poiList) {
        this.poiList = poiList;
    }

    public Map<LatLng, Element[]> getIntersectionOfStreets() {
        return intersectionOfStreets;
    }

    public void setIntersectionOfStreets(Map<LatLng, Element[]> intersectionOfStreets) {
        this.intersectionOfStreets = intersectionOfStreets;
    }
}
