package com.example.explore.model;

import com.google.gson.annotations.SerializedName;

public class Tag {
    @SerializedName("name")
    String name;

    @SerializedName("type")
    String type;

    @SerializedName("landuse")
    String landuse;

    @SerializedName("amenity")
    String amenity;

    @SerializedName("highway")
    String highway;

    @SerializedName("building")
    String building;

    @SerializedName("tourism")
    String tourism;

    @SerializedName("leisure")
    String leisure;

    @SerializedName("railway")
    String railway;

    @SerializedName("boundary")
    String boundary;

    @SerializedName("natural")
    String natural;

    @SerializedName("expressway")
    String expressway;

    @SerializedName("building:part")
    String buildingPart;

    @SerializedName("barrier")
    String barrier;

    @SerializedName("shop")
    String shop;

    @SerializedName("brand")
    String brand;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLanduse() {
        return landuse;
    }

    public void setLanduse(String landuse) {
        this.landuse = landuse;
    }

    public String getAmenity() {
        return amenity;
    }

    public void setAmenity(String amenity) {
        this.amenity = amenity;
    }

    public String getHighway() {
        return highway;
    }

    public void setHighway(String highway) {
        this.highway = highway;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getTourism() {
        return tourism;
    }

    public void setTourism(String tourism) {
        this.tourism = tourism;
    }

    public String getLeisure() {
        return leisure;
    }

    public void setLeisure(String leisure) {
        this.leisure = leisure;
    }

    public String getRailway() {
        return railway;
    }

    public void setRailway(String railway) {
        this.railway = railway;
    }

    public String getBoundary() {
        return boundary;
    }

    public void setBoundary(String boundary) {
        this.boundary = boundary;
    }

    public String getNatural() {
        return natural;
    }

    public void setNatural(String natural) {
        this.natural = natural;
    }

    public String getExpressway() {
        return expressway;
    }

    public void setExpressway(String expressway) {
        this.expressway = expressway;
    }

    public String getBuildingPart() {
        return buildingPart;
    }

    public void setBuildingPart(String buildingPart) {
        this.buildingPart = buildingPart;
    }

    public String getBarrier() {
        return barrier;
    }

    public void setBarrier(String barrier) {
        this.barrier = barrier;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
