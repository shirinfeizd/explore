package com.example.explore.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Element {

    @SerializedName("type")
    String type;

    @SerializedName("id")
    String id;
    @SerializedName("nodes")
    List<Long> nodesList;

    @SerializedName("geometry")
    List<Geometry> geometryList;

    @SerializedName("tags")
    Tag tag;

    @SerializedName("lat")
    Double latitude;

    @SerializedName("lon")
    Double longitude;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Geometry> getGeometryList() {
        return geometryList;
    }

    public void setGeometryList(List<Geometry> geometryList) {
        this.geometryList = geometryList;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tags) {
        this.tag = tags;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<Long> getNodesList() {
        return nodesList;
    }

    public void setNodesList(List<Long> nodesList) {
        this.nodesList = nodesList;
    }
}
