package com.example.explore.model;

import com.google.gson.annotations.SerializedName;

public class Geometry {
    @SerializedName("lat")
    Double latitude;
    @SerializedName("lon")
    Double longitude;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}

